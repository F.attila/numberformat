function formItem(seconds, duration, unit){
    let newUnit = '';
    let count = Math.floor(seconds/duration);
    if(count === 1){                                //singular
        newUnit += count + ' ' + unit;
    }else{                                          //plural
        newUnit += count + ' ' + unit + 's';
    }
    return newUnit;
}
function concat(seconds, duration, base, attachment){
    let tmp = base;
    if(seconds % duration === 0 && base.length > 1) {   //if it's the last component
        tmp += " and " + attachment;
    }else if(base.length > 1){                  //if it's not the first component and neither the last
        tmp += ", " + attachment;
    }else{
        tmp += attachment;
    }
    return tmp;
}

function formatDuration(seconds) {
    if(seconds < 0){
        return "Input must be positive";
    }
    if(seconds === 0){
        return "now";
    }
    let output = '';
    const min = 60;
    const hour = min * 60;
    const day = hour * 24;
    const year = day * 365;
    let tmpSeconds = seconds;
    if(tmpSeconds >= year){                                  //check if input is more than a year
        output += formItem(tmpSeconds, year, 'year'); //must be the first item => no concat function needed
        tmpSeconds -= (Math.floor(tmpSeconds/year) * year);
    }
    if(tmpSeconds >= day){
        output = concat(tmpSeconds, day, output, formItem(tmpSeconds, day, 'day'));
        tmpSeconds -= (Math.floor(tmpSeconds/day) * day);
    }
    if(tmpSeconds >= hour){
        output = concat(tmpSeconds, hour, output, formItem(tmpSeconds, hour, 'hour'));
        tmpSeconds -= (Math.floor(tmpSeconds/hour) * hour);
    }
    if(tmpSeconds >= min){
        output = concat(tmpSeconds, min, output, formItem(tmpSeconds, min, 'minute'));
        tmpSeconds -= (Math.floor(tmpSeconds/min) * min);
    }
    if(tmpSeconds >= 1){
        output = concat(tmpSeconds, 1, output, formItem(tmpSeconds, 1, 'second'));
    }
    return output;
}
